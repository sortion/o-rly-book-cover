# O'RLY? - Basic LaTeX Book Cover Template

Edit `cover.tex`, set your own title, subtitles, etc. and choose your beloved image line art (e.g. from [Pearson Scott Foresman Collection on Wikimedia Commons](https://commons.wikimedia.org/wiki/Commons:Pearson_Scott_Foresman/Complete_List)), then compile the document or include the relevant parts in your book cover page.
